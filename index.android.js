/**
 * routing and flexbox learning
 * https://github.com/facebook/react-native
 * by Zhe Xu
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import UsersListPage from './myPages/UsersListPage';
import UsersProfilePage from './myPages/UserProfilePage';
import UserPhotoPage from './myPages/UserPhotoPage';

const Flexbox_Routing = StackNavigator({
  Home: { screen: UsersListPage },
  Profile: {screen: UsersProfilePage},
  Photo: {screen:UserPhotoPage}
});

AppRegistry.registerComponent('Flexbox_Routing', () => Flexbox_Routing);
