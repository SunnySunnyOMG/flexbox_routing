# README #

### proj 总览 ###

* App -> UsersListPage.js -(click an user)-> UserProfilePage.js ->(loading posts, album and todos)-> render Tags ->(click an album) -> UserPhotoPages

![1.png](https://bitbucket.org/repo/Gggodj6/images/1227396994-1.png)![2.png](https://bitbucket.org/repo/Gggodj6/images/2852187493-2.png)![3.png](https://bitbucket.org/repo/Gggodj6/images/3130828908-3.png)![4.png](https://bitbucket.org/repo/Gggodj6/images/2141134601-4.png)





### 静态工具函数./myUtility/Utils.js说明 ###


```
#!javascript

# static fetchSomething(url, self, operator = '', index_name, buildIndexForList = true, safeLoading = false ,method = 'GET') # 
```

    This func can fecth online resouces and update views based on some params 
    
    @static
    @param {string} url                      : define the url of resouces
    @param {reference} self                  : define the reference of the original class/component
    @param {string} [operator='']            : define what we fetch data for:  (1) 'listView' or (2) default: something else
    @param {string} index_name               : define what key name is based on if we try to build an index
    @param {boolean} [buildIndexForList=true]: define if try to build an index
    @param {boolean} [safeLoading=false]     : define if render listView until its child loadingdone
    @param {string} [method='GET']           : denfine the REST method
    
    @memberof Utils

```
#!javascript

# static _buildIndex(objs, key_name) # 
```

    this func is to build an hash table based on given object array

    @static
    @param {Array} objs       : object array
    @param {string} key_name  : the key name that the index is based on
    @returns 

    @memberof Utils

### 重要组件./myComponents说明 ###

**<PostList />**

功能：

* 返回某用户全部posts， 以及对应的comments

自定属性： 

* userId ：定义需要显示相册的用户

* setNumber ：回调函数， 用于设定父组件 UserProfilePage.js 中标签的数字

**<TodoList />**

功能：

* 返回某用户全部Todos，支持点击条目切换 completed/uncompleted

自定属性：

* 同上

**<AlbumList />**

功能：

* 返回某用户全部相册的View，相册可点击进入新页面

自定属性：

* 同上+

* routeToPhotoPage： 回调函数， 用于navigate

**<MyButton />**

功能：

* 自定义按钮，比默认按钮有更多灵活性

自定属性：

* title： button上显示的名称

* viewStyle： button容器的style

* textStyle： button字体style


**<DisplayOneObject />**

功能：

* 按照 key：value的格式显示单个对象， 支持一个 key 对单个或者多个 value

自定属性：

* objectToDisplay: 要显示的对象

* ignoreList: 可定义需要忽略显示的key：vale； 默认： ['id','userId','postId']

* viewStyle：定义显示容器的style

* keyStyle: 定义key的文字style

* valueStyle: 定义value的文字style

### 下一步优化说明 ###

* 通过父子组件通讯， 页面通讯在根页面将数据保存，实现不重复fetch信息