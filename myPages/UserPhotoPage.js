/**
 * page for photos of a particulat album
 * by Zhe Xu
 */

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ListView,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

export default class UserPhotoPage extends Component{
  static navigationOptions = {
    title: 'Photos',
  };

  constructor(){
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    this.state = {
        dataSource: ds.cloneWithRows([]),

    };
  }

  componentWillMount(){
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(this.props.navigation.state.params),
    });
  }

  render(){
    return (
      <ListView
        enableEmptySections={true}
        dataSource = {this.state.dataSource}
        contentContainerStyle={styles.list}
        renderRow = {(rowData)=>{
          return(
            <TouchableHighlight
              onPress = {()=>this._onPress(rowData.url)}
              underlayColor= '#fff'
            >
              <View style = {styles.imgContainer}>
                <Image source={{uri: rowData.thumbnailUrl}} style={styles.photoThumbnail}/>
                <Text numberOfLines = {1} style = {styles.PhotoTitle}>{rowData.title}</Text>
              </View>
            </TouchableHighlight>
          );
        }}
      />
    );
  }

  _onPress(hdImgUrl){
    console.warn("press the image :"+hdImgUrl);
  }
}

const styles = StyleSheet.create({
  list:{
    flex:1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  imgContainer:{
    padding:5,
    width: Dimensions.get('window').width/3 ,
    height: Dimensions.get('window').width/3,
  },
  photoThumbnail:{
    flex:1,
    resizeMode:'cover',
    // android 里面图片支持圆角边界但是仍需要设置overflow属性； 注意：在图片上一级容器View中设置边界和overflow 无效
    borderRadius: 10,
    overflow: 'hidden',
  },
  PhotoTitle:{
    paddingLeft:10,
    paddingRight:10,
    color:'#000',
    fontWeight:'bold',
    fontFamily: 'Verdana',
  },
});