/**
 * page for a list of users
 * by Zhe Xu
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableHighlight,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import DisplayOneObject from '../myComponents/DisplayOneObject.js';
import Utils from '../myUtility/Utiils.js'


export default class UersList extends Component {
  static navigationOptions = {
    title: 'Users',
  };


 constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});

    this.state = { dataSource: ds.cloneWithRows([]) , 
                   postJson:[], 
                   commentJson: [],
                   comment:['empty!!!!'],
                   loadingDone: false,
                };              
  }  

  componentWillMount(){
    Utils.fetchSomething('https://jsonplaceholder.typicode.com/users', this,'listView','',false);
  }

  render() {
    //const {navigate} = this.props.navigation;
    return (
      <ListView 
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderRow={(rowData) => {
          return(
            <TouchableHighlight
              onPress = {()=>this._onPress(rowData)}
              underlayColor= '#333333'
            >
              <View style={styles.container}>
                <View>
                  <Image source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}} style={styles.avatar}>
                    <Text style ={styles.id}>userId: {rowData.id} </Text> 
                  </Image>             
                </View>

                <View style = {styles.infoBox}> 
                  <DisplayOneObject objectToDisplay = {rowData} keyStyle = {styles.key} />       
                </View>
              </View>
            </TouchableHighlight>
          )
          }
        }
      />
    );
  }

  _onPress(rowData){
    //console.warn('Press an user!');
    this.props.navigation.navigate('Profile', rowData);
  }

}

//加了一些样式 =。=
const styles = StyleSheet.create({
  list:{
    flex:1
  },
  container: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  infoBox:{
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  id:{
     color: '#ff1493',
     fontWeight:'bold',
  },
  key:{
    color:'#000000',
    fontWeight:'bold',
  },
  avatar:{
    marginRight:10,
    height:80,
    width:80,
  },
  buttonText:{
    padding:5,
    backgroundColor: '#0693e3',
    alignItems: 'flex-start',
    color:'#fff',
    fontWeight:'bold'
  },
  comment:{
    backgroundColor:'#d4c4fb',
    marginTop:10,
    padding:3,
  }
});





