/**
 * user profile page
 * by Zhe Xu
 */

import React, {Component, PropTypes} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Button,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import PostsList from '../myComponents/PostsList.js';
import AlbumList from '../myComponents/AlbumList.js';
import TodoList from '../myComponents/TodoList.js';
import MyButton from '../myComponents/MyButton.js';

export default class UserProfile extends Component{
  static navigationOptions = {
      title: 'Profile',
  };

  static PropTypes ={
    loadingForOnce : PropTypes.bool,
  }

  static get defaultProps(){
    return {
      loadingForOnce: false,
    }
    
  }

  constructor(){
    super();
    this.state={
      showWindowContent: <Text>Show something</Text>,
      //loadingForOnce: true,
      tagFocus:1,
      postNumber:0,
      albumNumber:0,
      todosNumber:0,
      followText: '+ \nFollow',
    }
  }

  componentWillMount(){
    this.setState({
      showWindowContent: <PostsList userId = {this.props.navigation.state.params.id} setNumber = {(number)=>this._setPostNumber(number)}/>,
    });
    this.toggleBool = true;
    // add some logic here to determine if have loaded posts, albums and todos
    if(true){
      this.loadingForOnce = true;
    }
    
  }

  render(){
    // The screen's current route is passed in to `props.navigation.state`:
    //const { params } = this.props.navigation.state;
    return(
      <ScrollView style={styles.wholePage} enableEmptySections={true}>
        <View style={styles.header}>

          {/*users avatar*/}
          <Image source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}} style={styles.avatar}/>   

          {/*users info*/}
          <View style = {styles.userInfo}>
            <View style = {styles.userInfoText}>
              <Text style={styles.userName}>{this.props.navigation.state.params.name}</Text>
              <Text style={styles.whatsUp} numberOfLines = {2}>
                Email: {this.props.navigation.state.params.email} {'\n' } 
                Phone: {this.props.navigation.state.params.phone}
              </Text>
            </View> 
            <MyButton viewStyle = {styles.followButtonView} 
                      textStyle = {styles.followButtonText} 
                      title     = {this.state.followText} 
                      onPress   = {()=>this._pressFollowButton()}/>       
          </View>  

        </View>

        <View  style={styles.body}>
          <View style={styles.tags}>
            {this.oneTag(1, this.state.postNumber  , 'Posts', 1==this.state.tagFocus)}
            {this.oneTag(2, this.state.albumNumber , 'Albums',2==this.state.tagFocus)}
            {this.oneTag(3, this.state.todosNumber , 'Todos', 3==this.state.tagFocus)}
          </View>
          
          <View style = {styles.showWindow}>
            <Text>date:2017-5-11</Text>  
            {this.state.showWindowContent}
          </View>
        </View>
        {this._loadingOnce()}
        <Text>{'\n' }</Text>
      </ScrollView>
    );

  }

// use a boolean: if_focus
  oneTag(index,number, item, if_focus){
    if(number>999){
      number = '>999';
    }
    return (
      <View style = {if_focus ? styles.tag_focus :styles.tag_blur} index={index}>
        <TouchableHighlight
          onPress = {()=>this._onPressTag(index)}
          underlayColor= '#fff'
        >
          <View >
            <Text style = {if_focus ? styles.tag_number_focus : styles.tag_number_blur}>{number}</Text>
            <Text style = {if_focus ? styles.tag_item_focus : styles.tag_item_blur}>{item}</Text>
          </View> 
        </TouchableHighlight>
      </View>
    );
  }



  _onPressTag(index){
    let content;
    switch(index){
      case 1:{
        //console.warn('tag 1');
        content = <PostsList userId = {this.props.navigation.state.params.id} 
                             setNumber = {(number)=>this._setPostNumber(number)}/>;                  
        break;
      }
      case 2:{
        //console.warn('tag 2');
        content = <AlbumList userId = {this.props.navigation.state.params.id} 
                             setNumber = {(number)=>this._setAlbumNumber(number)}
                             routeToPhotoPage={(photosArray)=>this._routeToPhotoPage(photosArray)}/>;       
        break;
      }
      case 3:{
        //console.warn('tag 3');
        content = <TodoList userId = {this.props.navigation.state.params.id} 
                            setNumber = {(number)=>this._setTodoNumber(number)}/>;
        break;
      }
      default:
      console.error('Undefined button');
    }
    this.setState({
      tagFocus: index,
      showWindowContent:  content,
    });

  }

  _setPostNumber(number){
    this.setState({
      postNumber: number,
    });
  }

  _setAlbumNumber(number){
    this.setState({
      albumNumber: number,
    });    
  }

  _setTodoNumber(number){
    this.setState({
      todosNumber: number,
    });    
  }

  _pressFollowButton(){
    this.toggleBool = !this.toggleBool;
    this.setState({
      followText:  this.toggleBool ? '+\nFollow':'-\nUnfollow',
    });
}
  _routeToPhotoPage(photosArray){
    //console.warn(JSON.stringify(photosArray));
    return this.props.navigation.navigate('Photo', photosArray);
  }

  _loadingOnce(){
      return (
        <View style={styles.invisible}>
          <View><AlbumList userId = {this.props.navigation.state.params.id} setNumber = {(number)=>this._setAlbumNumber(number)}/></View>
          <View><TodoList  userId = {this.props.navigation.state.params.id} setNumber = {(number)=>this._setTodoNumber(number)}/></View>       
        </View>
      );
    }


 
}

const styles = StyleSheet.create({

  wholePage: {
    flex: 1,
    padding: 10,
    // WHY cannot set these style here????
    //justifyContent: 'flex-start',
   // alignItems: 'flex-start',
    backgroundColor: '#fff',
  },
  header:{
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start', 
    borderBottomWidth: 0.5,
  },
  body:{
    flex:2,
    //backgroundColor:'#888888'
    flexDirection: 'column',
   // borderWidth: 0.5,
  },
  avatar:{
    height: 90,
    flex:1.5,
    width: 90,
    borderRadius: 5,
  },
  userInfo:{
    flex:1,
    flexDirection: 'row',
    //alignItems: 'flex-end',
    paddingBottom: 10,
    paddingTop:5,
    //borderWidth:1,
  },
  userInfoText:{
    flex:3,
    paddingRight: 25,
    justifyContent:'space-between'
  },
  followButtonView:{
    flex:1,
    justifyContent:'flex-end',
    backgroundColor: '#0693e3',
    width: 70,
    padding:5,
    paddingBottom:10,
    borderRadius:10,
    //elevation:15,
  },
  followButtonText:{
    color:'#fff',
    fontWeight:'bold',
  },
  userName:{
    fontWeight:'bold',
    fontSize: 23,
    color: '#000',
    paddingBottom:3,
  },
  whatsUp:{

  },
  tags:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    padding:5,
    marginTop:5,
  },
  tag_blur:{
    flex:1,
    padding:5,
    paddingLeft:10,
    backgroundColor:'#fff',
  },
  tag_focus:{
    flex:1,
    padding:5,
    elevation:5,
    backgroundColor:'#fff',
    paddingLeft:10,
  },
  tag_number_blur:{
    fontSize: 28,
    color: '#000',
    fontFamily:'Helvetica',
  },
  tag_item_blur:{
    fontSize: 20,
  },
  tag_number_focus:{
    fontSize: 28,
    color: '#0693e3',
    fontFamily:'Helvetica',
  },
  tag_item_focus:{
    fontSize: 20,
    color:'#0693e3',
  },
  
  showWindow:{
    flex: 4,
    backgroundColor: '#fff',
  },
  albumImage:{
    flex:1
  },
  invisible:{
    height:1,
    width:1,
    overflow:'hidden',
  }
});

