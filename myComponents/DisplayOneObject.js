/**
 * @param oneObject 
 * Component to show/hide comments
 * by Zhe Xu
 * @return display the props: value of the objects
 */
import React, { Component ,PropTypes} from 'react';
import {
  Text,
  View,
} from 'react-native';


class DisplayOneObject extends Component {
  static propTypes ={
    objectToDisplay: PropTypes.object.isRequired,
    ignoreList: PropTypes.array,
    keyStyle: Text.propTypes.style,
    valueStyle: Text.propTypes.style,
    viewStyle: View.propTypes.style,
  };

 static get defaultProps() {
  return {
    keyStyle: {
      color:'#000000', 
      fontWeight:'bold',
    },
    valueStyle:{
      flex:1, flexDirection: 'column',
    },
    ignoreList: ['id','postId','userId'],
  } 
};

  render(){
   return(
     <View style={this.props.viewStyle}>
      { this._display(this.props.objectToDisplay, this.props.keyStyle, this.props.valueStyle) }
    </View>
   );
    
  }

_display(obj, keyStyle, valueStyle){
   if (typeof obj === 'object'){   
     var textArray =[];
      for(var props in obj){
          if(!this._onIgnoreList(props))
            textArray.push((
            <Text key = {props} style = {valueStyle}>
              <Text style={keyStyle}>{props}: </Text>{this._display(obj[props])} 
            </Text>));
        }
     return textArray;
    }
    else
     return obj.toString();
  } 

  _onIgnoreList(props){
    // dismiss some props 
    for(var index in this.props.ignoreList){
      if (this.props.ignoreList[index] == props)
        return true;
    }
    return false;
  }

}

module.exports = DisplayOneObject;