/**
 * to implement a album window
 */
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableHighlight,
} from 'react-native';
import DisplayObjects from './DisplayObjects.js';
import DisplayOneObject from './DisplayOneObject.js';
import Utils from '../myUtility/Utiils.js'

export default class TodoList extends Component {
    static PropTypes={
      userId: PropTypes.number.isRequired,
  }

 constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    this.state = { dataSource: ds.cloneWithRows([]) , 
                   listViewHash: {}, 
                   loadingDone: false,
                };              
  }

  componentWillMount(){
    Utils.fetchSomething('https://jsonplaceholder.typicode.com/todos', this,'listView','userId',true);
  }

  render(){
    return(
      <ListView 
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderRow={(rowData) => {
          return(
            <TodoTag completed = {rowData.completed} title = {rowData.title}/>
          )
          }
        }
      />
    );
  }
}

class TodoTag extends Component{
  static PropTypes ={
    title: PropTypes.string.isRequired,
    completed: PropTypes.bool,
  }

  static get defaultProps(){
    return{
      completed: false,
    }  
  }

  constructor(){
    super();
    this.state ={
      completed: false,
    };
  }

  componentWillMount(){
    this.setState({
      completed: this.props.completed,
    });
  }

  render(){
    return (
      <TouchableHighlight
        onPress = {()=>{ this.setState({completed: !this.state.completed,}); }}
        underlayColor = '#ff8a65'
        
      >
          <View style = {styles.title}>
            <Text style = {this.state.completed ? styles.titleTextCompleted : styles.titleTextUncompleted}>
                {this.props.title}
            </Text>
          </View>
      </TouchableHighlight>
    );
  }

  
  _onPress(flag){

  }
}

//加了一些样式 =。=
const styles = StyleSheet.create({
  title:{
    margin:5,
    padding: 5,
    //borderWidth: 1,
    elevation:3,
    backgroundColor: '#d4c4fb',
    borderRadius: 5,
  },
  titleTextCompleted:{
    fontSize: 17,
    color:'#00d084',
    textDecorationLine: 'line-through',
  },
  titleTextUncompleted:{
    fontSize: 17,
    color:'#000000',
  },
});


module.exports = TodoList;