/**
 * to implement a album window
 */
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import DisplayObjects from './DisplayObjects.js';
import DisplayOneObject from './DisplayOneObject.js';
import Utils from '../myUtility/Utiils.js';

export default class AlbumList extends Component {
    static PropTypes={
      userId: PropTypes.number.isRequired,
  }

 constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    this.state = { dataSource: ds.cloneWithRows(['Loading...']) , 
                   listViewHash:{}, 
                   childHash: {},
                   loadingDone: false,
                };              
  }

  componentWillMount(){
    Utils.fetchSomething('https://jsonplaceholder.typicode.com/albums',this,'listView','userId');
    Utils.fetchSomething('https://jsonplaceholder.typicode.com/photos',this,'','albumId');
  }

  render(){
    return(
      <ListView 
        contentContainerStyle={styles.list}
        dataSource={this.state.dataSource}
        renderRow={(rowData) => {
          return(
            this._renderRow(rowData)
          )
          }
        }
      />
    );
  }

  _renderRow(rowData){
    return(
    <TouchableHighlight
      onPress = {()=>this._onPressImg(rowData.id)}
      underlayColor= '#fff'
    >
      <View style = {styles.albumInfo}> 
        <Image source={{uri:  this._findThumbNailUrlBy(rowData.id)}} style= {styles.thumbnail}/>
      </View>     
    </TouchableHighlight>
    );

  }

  _findThumbNailUrlBy(albumId){
    if (!this.state.loadingDone)
      //an image to show loading status
      return ('http://www.jqueryscript.net/images/Easy-To-Customize-jQuery-Loading-Indicator-Plugin-babypaunch-spinner-js.jpg');

    return this.state.childHash[albumId.toString()][0].thumbnailUrl;
  }

  _onPressImg(albumId){
    this.props.routeToPhotoPage(this.state.childHash[albumId.toString()]);
  }
}

//加了一些样式 =。=
const styles = StyleSheet.create({
  list: {
    marginTop:5,
    justifyContent: 'space-around',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  albumInfo:{
    padding: 3,
    width: Dimensions.get('window').width/2 - 10,
    height:Dimensions.get('window').height/4 - 20, 
    backgroundColor:'#fff',
    // 这是一个诡异的坑！！！！！！！！！！！！！！！！！！！！！！
    //alignItems: 'flex-start',

    // 在设置宽和高的情况下，不能使用flex
    //flex:1
  },
  avatar:{
    height: 100,
    flex:1.5,
    width: 100,

  },
  thumbnail:{
    flex:1,
    resizeMode:'cover',
    // android 里面图片支持圆角边界但是仍需要设置overflow属性； 注意：在图片上一级容器View中设置边界和overflow 无效
    borderRadius: 10,
    overflow: 'hidden',
    //borderWidth: 3,
  },
  //在 image 中的 元素， 会在image加载过后，再加载，但注意：此次加载会重载一遍image，可能会丢失之前image的style
  imageTextView:{
    flex:1,
    borderRadius: 20,
    overflow: 'hidden',
    borderWidth: 1,
  },
  id:{
     color: '#ff1493',
     fontWeight:'bold',
  },
  key:{
    color:'#fff',
    fontWeight:'bold',
  },
  value:{
    borderRadius: 20,
    overflow: 'hidden',
    color:'#fff'
  },
  comment:{
    backgroundColor:'#d4c4fb',
    marginTop:10,
    padding:3,
  }
});


module.exports = AlbumList;