/**
 * a customized button
 */

import React, { Component ,PropTypes} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
} from 'react-native';

// acept 3 props : viewStyle, textStyle,title
class MyButton extends Component {
  render(){
    return(
      
        <TouchableHighlight
          onPress={()=>this.props.onPress()}
          underlayColor= '#fff' 
        >
         <View style = {this.props.viewStyle}>
           <Text style = {this.props.textStyle}>{this.props.title}</Text>
         </View>
        </TouchableHighlight>
      
    );
  }
}
module.exports = MyButton;