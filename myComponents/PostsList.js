/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
} from 'react-native';
//import  Accordion from 'react-native-accordion';
import DisplayComment from './DisplayComment.js';
import DisplayOneObject from './DisplayOneObject.js';
import DisplayObjects from './DisplayObjects.js';
import Utils from '../myUtility/Utiils.js';


export default class PostsList extends Component {

  static PropTypes={
    userId: PropTypes.number.isRequired,
  }

// 构造函数声明所需的变量; 获取所需的数据
 constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    this.state = { dataSource: ds.cloneWithRows([]) , 
                   listViewHash:{}, 
                   childHash: {},
                   loadingDone: false,
                };              
  }

  componentWillMount(){
    if(true){
      Utils.fetchSomething('https://jsonplaceholder.typicode.com/comments',this,'','postId',true, true);  
      Utils.fetchSomething('https://jsonplaceholder.typicode.com/posts', this,'listView','userId',true, true);
    }
    else{
      // no need for fecth 
    }

  }

  render() {
    return (
      <ListView 
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderRow={(rowData) => {
          return(
            <View style = {styles.infoBox}> 
              <DisplayOneObject objectToDisplay = {rowData} keyStyle = {styles.key} /> 
              {this.renderComment(this.getComments(rowData.id))}
            </View>
          )
          }
        }
      />
    );
  }

   getComments(postId){
    if (!this.state.loadingDone)
      return [{'status':' Loading now...'}];

    if(postId){   
      return this.state.childHash[postId.toString()];
    }   
    else{
      return [{'status':' Error! Cannot find a valid postId!'}];
    }
      
   }

   renderComment(objs){
      var header = (
      <View>
        <Text style={styles.buttonText} >COMMENTS</Text>
      </View>
      );
      
      var content = (
      <View>
        <DisplayObjects objectsToDisplay={objs}/>
      </View>
      );     

       return (
        <DisplayComment
          underlayColor = '#fff'   
          header={header}
          content={content}
          //onPress = {this._onPress}
        />
      );
  }

  _onPress(){
    //could define more operation here...
    console.warn('onPress operation'); 
  }
}


//加了一些样式 =。=
const styles = StyleSheet.create({
  list: {
    marginTop:5,
    justifyContent: 'space-around',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  infoBox:{
    flex: 1,
    // 这是一个诡异的坑！！！！！！！！！！！！！！！！！！！！！！
    //alignItems: 'flex-start',
    //borderWidth: 1,
    padding:5,
    overflow:'hidden',
    backgroundColor:'#fff',
    borderBottomWidth:2,
  },
  id:{
     color: '#ff1493',
     fontWeight:'bold',
  },
  key:{
    color:'#000000',
    fontWeight:'bold',
  },
  buttonText:{
    padding:5,
    backgroundColor: '#0693e3',
    alignItems: 'flex-start',
    color:'#fff',
    fontWeight:'bold'
  },
  comment:{
    backgroundColor:'#d4c4fb',
    marginTop:10,
    padding:3,
  }
});

module.exports = PostsList;


class Star extends Component {
  render() {
    return (
      <IconEntypo
        style = {this.props.selected? styles.star_selected : styles.star_unselected}
        name = {this.props.selected? 'star' : 'star-outlined'}/>
    );
  }
}



