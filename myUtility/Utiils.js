export default class Utils{
  /**fetchSomething
   * This func can fecth online resouces and update views based on some params 
   * 
   * @static
   * @param {string} url define the url of resouces
   * @param {reference} self define the reference of the original class/component
   * @param {string} [operator=''] define what we fetch data for:  (1) 'listView' or (2) default: something else
   * @param {string} index_name define what key name is based on if we try to build an index
   * @param {boolean} [buildIndexForList=true] define if try to build an index
   * @param {boolean} [safeLoading=false] define if not render listView until its child loading done
   * @param {string} [method='GET'] denfine the REST method
   * 
   * @memberof Utils
   */
  static fetchSomething(url, self, operator = '', index_name, buildIndexForList = true, safeLoading = false ,method = 'GET'){
    fetch(url, {method:method,})
      .then( (response) => response.json())// convert to Json, return a promise
      .then((responseJ)=>{
     if (operator == 'listView'){
       // build index?
       if(buildIndexForList){
          tempHash = Utils._buildIndex(responseJ,index_name);
          self.props.setNumber(tempHash[self.props.userId.toString()].length);
          //use safeLoading? (wait for comments loading done)
          if(safeLoading){
            self.setState({
              listViewHash: tempHash,
              dataSource: self.state.dataSource.cloneWithRows(self.state.childHash['1'] ? tempHash[self.props.userId.toString()] : []),
             });
             //console.warn(JSON.stringify(self.state.childHash['1']) +!!self.state.childHash )
          }
          else{
            self.setState({
              listViewHash: tempHash,
              dataSource: self.state.dataSource.cloneWithRows(tempHash[self.props.userId.toString()]),
             });
          }
       }                
       else{
         self.setState({
           dataSource: self.state.dataSource.cloneWithRows(responseJ),
         });
       }

      }    
      // do not build a listView
     else{
        tempHash =Utils._buildIndex(responseJ,index_name);
        if(safeLoading&&self.state.listViewHash['1']){
          self.setState( { 
            dataSource: self.state.dataSource.cloneWithRows(self.state.listViewHash[self.props.userId.toString()]),
            childHash: tempHash,
            loadingDone: true, 
          });
        }
        else{
            self.setState( { 
            childHash: tempHash,
            loadingDone: true, 
          });
        }

      //console.warn('comments loading done!')
       //console.warn(JSON.stringify(this.state.commentJson));
     }
     })
      .catch((error) => {
          console.error(error);
        }); 
  }
/**
 * this func is to build an hash table based on given object array
 * 
 * @static
 * @param {Array} objs the object array
 * @param {string} key_name the key name that the index is based on
 * @returns 
 * 
 * @memberof Utils
 */
    static _buildIndex(objs, key_name){
     let objHash = {};
     objs.forEach(obj => {
       if(!obj[key_name])
         console.warn('Cannot find prop: '+key_name);  

       let index_name = obj[key_name];
       if (objHash[index_name]){
         objHash[index_name].push(obj);
       }
       else{
         objHash[index_name] = [];
         objHash[index_name].push(obj);
       }
     })
     //this.commentHash = objHash;
     return objHash;
   }
}
